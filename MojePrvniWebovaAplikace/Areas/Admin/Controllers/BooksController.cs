﻿using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using MojePrvniWebovaAplikace.Class;

namespace MojePrvniWebovaAplikace.Areas.Admin.Controllers
{
    [Authorize]
    public class BooksController : Controller
    { // GET: Books
        //"int? page" je tzv. "nullable", neboli nepovinný parametr.
        public ActionResult Index(int? page)
        {
            int itemsOnPage = 1;
            int pg = page.HasValue ? page.Value : 1;
            int totalBooks;

            ViewBag.Datum = DateTime.Now.ToShortDateString();

            BookDao bookDao = new BookDao();
            IList<Book> books = bookDao.GetBooksPaged(itemsOnPage, pg, out totalBooks);

            ViewBag.Pages =(int) Math.Ceiling((double) totalBooks /(double)itemsOnPage);
            ViewBag.Categories = new BookCategoryDao().GetAll();
            ViewBag.CurrentPage = pg;

            KnihovnaUser user = new KnihovnaUserDao().GetByLogin(User.Identity.Name);

            if (user.Role.Identificator == "ctenar")
            {
                return View("IndexCtenar", books);
            }

            return View(books);
        }
        //TODO: Opravit nefunkční vyhledávání v kategorii (prázdná proměnná "categoryId")!
        public ActionResult Category(int categoryId)
        {
            IList<Book> books = new BookDao().getBooksCategoryId(categoryId);
            ViewBag.Categories = new BookCategoryDao().GetAll();
            return View("IndexCtenar",books);
        }

        public ActionResult Search(string phrase)
        {
            BookDao bookDao = new BookDao();
            IList<Book> books = bookDao.SearchBooks(phrase);

            return View("IndexCtenar",books);
        }
        //Otazníkem (v případě Booleanu v metodě níže) se označuje parametr, jako "nullable", tedy nemusí se zadávat během volání tohoto pohledu a může být prázdný "null".
        public ActionResult Detail(int id)
        {
            BookDao bookDao = new BookDao();
            Book b = bookDao.GetById(id);


            return View(b);
        }
        [Authorize(Roles = "knihovnik")]
        public ActionResult Create()
        {
            BookCategoryDao bookCategoryDao = new BookCategoryDao();
            IList<BookCategory> categories = bookCategoryDao.GetAll();
            ViewBag.Categories = categories;

            return View();
        }
        //V hranaté závorce "[HttpPost]" definuji, že danou metodu (pod závorkami) bude možné spouštět pouze prostřednictvím formuláře využívajícího metodu "Post".
        [HttpPost]
        [Authorize(Roles = "knihovnik")]
        public ActionResult Add(Book book, HttpPostedFileBase picture, int categoryId)
        {
            if (ModelState.IsValid)
            {




                if (picture != null)
                {
                    if (picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {
                        Image image = Image.FromStream(picture.InputStream);
                        if (image.Height > 200 || image.Width > 200)
                        {
                            Image smallImage = ImageHelper.ScaleImage(image, 200, 200);
                            Bitmap b = new Bitmap(smallImage);

                            Guid guid = Guid.NewGuid();
                            string imageName = guid.ToString() + ".jpg";
                            b.Save(Server.MapPath("~/Uploads/Book/" + imageName), ImageFormat.Jpeg);
                            smallImage.Dispose();
                            b.Dispose();

                            book.ImageName = imageName;
                        }
                        else
                        {
                            picture.SaveAs(Server.MapPath("~/Uploads/Book/") + picture.FileName);
                        }
                    }
                }

                BookCategoryDao bookCategoryDao = new BookCategoryDao();
                BookCategory bookCategory = bookCategoryDao.GetById(categoryId);

                book.Category = bookCategory;

                BookDao bookDao = new BookDao();
                bookDao.Create(book);

                TempData["message-success"] = "Kniha byla úspěšně přidána.";
            }
            else
            {
                return View("Create", book);
            }

            return RedirectToAction("Index");

        }
        [Authorize(Roles = "knihovnik")]
        public ActionResult Edit(int id)
        {
            BookDao bookDao = new BookDao();
            BookCategoryDao bookCategoryDao = new BookCategoryDao();

            Book b = bookDao.GetById(id);

            ViewBag.Categories = bookCategoryDao.GetAll();

            return View(b);
        }
        [HttpPost]
        public ActionResult Update(Book book, HttpPostedFileBase picture, int categoryId)
        {
            try
            {
                BookDao bookDao = new BookDao();
                BookCategoryDao bookCategoryDao = new BookCategoryDao();

                BookCategory bookCategory = bookCategoryDao.GetById(categoryId);
                book.Category = bookCategory;

                if (picture != null)
                {
                    if (picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {
                        Image image = Image.FromStream(picture.InputStream);

                        Guid guid = Guid.NewGuid();
                        string imageName = guid.ToString() + ".jpg";

                        if (image.Height > 200 || image.Width > 200)
                        {
                            Image smallImage = ImageHelper.ScaleImage(image, 200, 200);
                            Bitmap b = new Bitmap(smallImage);


                            b.Save(Server.MapPath("~/Uploads/Book/" + imageName), ImageFormat.Jpeg);
                            smallImage.Dispose();
                            b.Dispose();


                        }
                        else
                        {
                            picture.SaveAs(Server.MapPath("~/Uploads/Book/") + picture.FileName);
                        }

                        System.IO.File.Delete(Server.MapPath("~/Uploads/Book" + book.ImageName));

                        book.ImageName = imageName;
                    }
                }

                bookDao.Update(book);


                TempData["message-success"] = "Kniha " + book.Name + " byla upravena.";
            }
            catch (Exception e)
            {
                throw;

            }



            return RedirectToAction("Index", "Books");
        }

        [Authorize(Roles = "knihovnik")]
        public ActionResult Delete(int id)
        {
            try
            {
                BookDao bookDao = new BookDao();
                Book book = bookDao.GetById(id);

                System.IO.File.Delete(Server.MapPath("~/Uploads/Book" + book.ImageName));
                bookDao.Delete(book);

                TempData["message-success"] = "Kniha " + book.Name + " byla úspěšně smazána.";
            }
            catch (Exception e)
            {
                throw;
            }

            return RedirectToAction("Index");
        }
    }
}