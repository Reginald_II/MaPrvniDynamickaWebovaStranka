﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using DataAccess.Model;

namespace MojePrvniWebovaAplikace.Areas.Admin.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
        [ChildActionOnly]
        // GET: Admin/Menu
        public ActionResult Index()
        {

            KnihovnaUserDao knihovnaUserDao = new KnihovnaUserDao();
            KnihovnaUser user = knihovnaUserDao.GetByLogin(User.Identity.Name);

            return View(user);
        }
    }
}