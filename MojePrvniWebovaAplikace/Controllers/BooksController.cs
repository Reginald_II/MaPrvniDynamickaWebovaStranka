﻿using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using MojePrvniWebovaAplikace.Class;

namespace MojePrvniWebovaAplikace.Controllers
{
    
    public class BooksController : Controller
    {
        // GET: Books
        public ActionResult Index()
        {
            ViewBag.Datum = DateTime.Now.ToShortDateString();

            BookDao bookDao = new BookDao();
            IList<Book> books = bookDao.GetAll();
            return View(books);
        }
        //Otazníkem (v případě Booleanu v metodě níže) se označuje parametr, jako "nullable", tedy nemusí se zadávat během volání tohoto pohledu a může být prázdný "null".
        public ActionResult Detail(int id)
        {
            BookDao bookDao = new BookDao();
            Book b = bookDao.GetById(id);
           

            return View(b);
        }
     
    }

    }
    

