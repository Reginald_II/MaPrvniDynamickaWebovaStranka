﻿using DataAccess.Model;

namespace DataAccess.Dao
{
    public class BookCategoryDao : DaoBase<BookCategory>
    {
        //Konstruktor níže je konstruktor, který přes ": base" dědí od předka.
        public BookCategoryDao() : base()
        {

        }
    }
}
