﻿using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class KnihovnaUserDao : DaoBase<KnihovnaUser>
    {
        public KnihovnaUser GetByLoginAndPassword(string login, string password)
        {
            return session.CreateCriteria<KnihovnaUser>().Add(Restrictions.Eq("Login", login))
                .Add(Restrictions.Eq("Password", password))
                .UniqueResult<KnihovnaUser>();
        }
        public KnihovnaUser GetByLogin(string login)
        {
            return session.CreateCriteria<KnihovnaUser>().Add(Restrictions.Eq("Login", login))
                .UniqueResult<KnihovnaUser>();
        }
    }
}