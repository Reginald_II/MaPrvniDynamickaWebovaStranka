﻿using DataAccess.Interface;

namespace DataAccess.Model
{
    public class KnihovnaRole : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Identificator { get; set; }
        public virtual string RoleDescription { get; set; }

    }
}
