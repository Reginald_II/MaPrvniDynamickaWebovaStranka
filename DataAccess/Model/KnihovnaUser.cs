﻿using System.ComponentModel.DataAnnotations;
using System.Web.Security;
using DataAccess.Interface;

namespace DataAccess.Model
{
    //Dědím od třídy "MembershipUser", která spravuje role uživatelů. Lepší Alternativou je sice Windows identity, ale kvůli její přílišné komplikovanosti zde použita nebude.
    public class KnihovnaUser : MembershipUser, IEntity
    {
        public virtual int Id { get; set; }
        [Required (ErrorMessage = "Jméno je vyžadováno!")]
        public virtual string Name { get; set; }
        [Required(ErrorMessage="Přijmení je vyžadováno!")]
        public virtual string Surname { get; set; }
        [Required(ErrorMessage = "Login je vyžadován!")]
        public virtual string Login { get; set; }
        [Required(ErrorMessage = "Heslo je vyžadováno!")]
        public virtual string Password { get; set; }
        public virtual KnihovnaRole Role { get; set; }
    }
}
