﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Book : IEntity
    {
        public virtual BookCategory Category { get; set; }
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Název knihy je vyžadován!")]
        public virtual string Name { get; set; }
        [Required(ErrorMessage ="Autor je vyžadován!")]
        public virtual string Author { get; set; }
        [Required(ErrorMessage ="Rok vydání je vyžadován!")]
        [Range(0,2100, ErrorMessage = "Rok může být od 0-2100!")]
        public virtual int PublishedYear { get; set; }
              
        [AllowHtml]
        public virtual string Description { get; set; }

        public virtual string ImageName { get; set; }
    }
}
