﻿using DataAccess.Interface;

namespace DataAccess.Model
{
   public class BookCategory : IEntity
    {
        //Atributy níže jsou virtuální, což umožňuje jejich přetížení ("@override") v jiných třídách.
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set;}



    }
}
