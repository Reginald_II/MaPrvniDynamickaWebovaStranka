﻿namespace DataAccess.Interface
{
    public interface IEntity
    {
    int Id { get; set; }
    }
}
